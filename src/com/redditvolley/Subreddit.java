package com.redditvolley;
import org.json.*;

public class Subreddit
{
	private final int	accountsActive;	//number of users active in last 15 minutes
	private final String	description;	//sidebar text
	private final String	descriptionHtml;	//sidebar text, escaped HTML format
	private final String	displayName;	//human name of the subreddit
	private final String	headerImg;	//full URL to the header image, or null
	private final JSONArray	headerSize;	//width and height of the header image, or null
	private final String	headerTitle;	//description of header image shown on hover, or null
	private final boolean	over18;	//is_nsfw?
	private final String	publicDescription;	//Description shown in subreddit search results?
	private final long	subscribers;	//the number of redditors subscribed to this subreddit
	private final String	title;	//title of the main page
	private final String	url;	//The relative URL of the subreddit. Ex: "/r/pics/"
	
	public Subreddit(JSONObject obj) {
		accountsActive = obj.optInt(Const.ACCOUNTSACTIVE);
		description = obj.optString(Const.DESCRIPTION);
		descriptionHtml = obj.optString(Const.DESCRIPTIONHTML);
		displayName = obj.optString(Const.DISPLAYNAME);
		headerImg = obj.optString(Const.HEADERIMG);
		headerSize = obj.optJSONArray(Const.HEADERSIZE);
		headerTitle = obj.optString(Const.HEADERTITLE);
		over18 = obj.optBoolean(Const.OVER18);
		publicDescription = obj.optString(Const.PUBLICDESCRIPTION);
		subscribers = obj.optLong(Const.SUBSCRIBERS);
		title = obj.optString(Const.TITLE);
		url = obj.optString(Const.URL);
		
	}
}
