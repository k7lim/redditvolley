package com.redditvolley;
import org.json.*;

public class Link
{
	private final String	author;//	the account name of the poster. null if this is a promotional link
	private final Flair	authorFlair; //_css_class	the CSS class of the author's flair. subreddit specific
	//String	author_flair_text	the text of the author's flair. subreddit specific
	private final boolean	clicked;	//probably always returns false
	private final String	domain;	//the domain of this link. Self posts will be self.reddit.com while other examples include en.wikipedia.org and s3.amazon.com
	private final boolean	hidden;	//true if the post is hidden by the logged in user. false if not logged in or not hidden.
	private final boolean	isSelf;	//true if this link is a selfpost
	private final boolean	likes;	//how the logged-in user has voted on the link - True = upvoted, False = downvoted, null = no vote
	private final Flair linkFlair; //link_flair_css_class	the CSS class of the link's flair.
	//String	link_flair_text	the text of the link's flair.
	private final JSONObject	media;	//Used for streaming video. Detailed information about the video and it's origins are placed here
	private final JSONObject	mediaEmbed;	//Used for streaming video. Technical embed specific information is found here.
	private final int	numComments;	//the number of comments that belong to this link. includes removed comments.
	private final boolean	over18;	//true if the post is tagged as NSFW. False if otherwise
	private final String	permalink;	//relative url of the permanent link for this link
	private final boolean	saved;	//true if this post is saved by the logged in user
	private final int	score;	//the net-score of the link. note: A submission's score is simply the number of upvotes minus the number of downvotes. If five users like the submission and three users don't it will have a score of 2. Please note that the vote numbers are not "real" numbers, they have been "fuzzed" to prevent spam bots etc. So taking the above example, if five users upvoted the submission, and three users downvote it, the upvote/downvote numbers may say 23 upvotes and 21 downvotes, or 12 upvotes, and 10 downvotes. The points score is correct, but the vote totals are "fuzzed".
	private final String	selftext;	//the raw text. this is the unformatted text which includes the raw markup characters such as ** for bold. <, >, and & are escaped. Empty if not present.
	private final String	selftextHtml;	//the formatted escaped html text. this is the html formatted version of the marked up text. Items that are boldened by ** or *** will now have <em> or *** tags on them. Additionally, bullets and numbered lists will now be in html list format. NOTE: The html string will be escaped. You must unescape to get the raw html. Null if not present.
	private final String	subreddit;	//subreddit of thing excluding the /r/ prefix. "pics"
	private final String	subredditId;	//the id of the subreddit which is the thing is located in
	private final String	thumbnail;	//full url to the thumbnail for this link; "self" if this is a self post
	private final String	title;	//the title of the link. may contain newlines for some reason
	private final String	url;	//the link of this post. the permalink if this is a self-post
	private final long	edited;	//Indicates if link has been edited. Will be the edit timestamp if the link has been edited and return false otherwise. https://github.com/reddit/reddit/issues/581
	private final String	distinguished;	//to allow determining whether they have been distinguished by moderators/admins. null = not distinguished. moderator = the green [M]. admin = the red [A]. special = various other special distinguishes http://bit.ly/ZYI47B
	private final boolean	stickied;	//true if the post is set as the sticky in its subreddi
	
	public Link(JSONObject obj) {
		author = obj.optString(Const.AUTHOR);
		authorFlair = new Flair(obj);
		clicked = obj.optBoolean(Const.CLICKED);
		domain = obj.optString(Const.DOMAIN);
		hidden = obj.optBoolean(Const.HIDDEN);
		isSelf = obj.optBoolean(Const.ISSELF);
		Object tmpLikes = obj.opt(Const.LIKES);
		likes = tmpLikes == null ? null : obj.optBoolean(Const.LIKES);
		linkFlair = new Flair(obj, "link");
		media = obj.optJSONObject(Const.MEDIA);
		mediaEmbed = obj.optJSONObject(Const.MEDIAEMBED);
		numComments = obj.optInt(Const.NUMCOMMENTS);
		over18 = obj.optBoolean(Const.OVER18);
		permalink = obj.optString(Const.PERMALINK);
		saved = obj.optBoolean(Const.SAVED);
		score = obj.optInt(Const.SCORE);
		selftext = obj.optString(Const.SELFTEXT);
		selftextHtml = obj.optString(Const.SELFTEXTHTML);
		subreddit = obj.optString(Const.SUBREDDIT);
		subredditId = obj.optString(Const.SUBREDDITID);
		thumbnail = obj.optString(Const.THUMBNAIL);
		title = obj.optString(Const.TITLE);
		url = obj.optString(Const.URL);
		Object tmpEdited = obj.opt(Const.EDITED);
		edited = tmpEdited == null ? null : obj.optLong(Const.EDITED);
		distinguished = obj.optString(Const.DISTINGUISHED);
		stickied = obj.optBoolean(Const.STICKIED);
	}
}
