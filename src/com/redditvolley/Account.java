package com.redditvolley;
import org.json.*;

public class Account
{
	final int	commentKarma;	//user's comment karma
	final long	created; //Registration date in epoch-seconds, local
	final long	createdUtc;	//registration date in epoch-seconds, UTC
	final boolean	hasMail;	//user has unread mail? null if not your account
	final boolean	hasModMail;	//user has unread mod mail? null if not your account
	final boolean	hasVerifiedEmail;	//user has provided an email address and got it verified?
	final String	id;	//ID of the account; prepend t2_ to get fullname
	final boolean	isFriend;	//whether the logged-in user has this user set as a friend
	final boolean	isGold;	//reddit gold status
	final boolean	isMod;	//whether this account moderates any subreddits
	final int	linkKarma;	//user's link karma
	final String	modhash;	//current modhash. not present if not your account
	final String	name;	//The username of the account in question. This attribute overrides the superclass's name attribute. Do not confuse an account's name which is the account's username with a thing's name which is the thing's FULLNAME. See API: Glossary for details on what FULLNAMEs are.
	final boolean	over18;	//whether this account is set to be over 18
	
	public Account(JSONObject obj) {
		commentKarma = obj.optInt(Const.COMMENTKARMA);
		created = obj.optLong(Const.CREATED);
		createdUtc = obj.optLong(Const.CREATEDUTC);
		hasMail = obj.optBoolean(Const.HASMAIL);
		hasModMail = obj.optBoolean(Const.HASMODMAIL);
		hasVerifiedEmail = obj.optBoolean(Const.HASVERIFIEDEMAIL);
		id = obj.optString(Const.ID);
		isFriend = obj.optBoolean(Const.ISFRIEND);
		isGold = obj.optBoolean(Const.ISGOLD);
		isMod = obj.optBoolean(Const.ISMOD);
		linkKarma = obj.optInt(Const.LINKKARMA);
		modhash = obj.optString(Const.MODHASH);
		name = obj.optString(Const.NAME);
		over18 = obj.optBoolean(Const.OVER18);
	}
}
