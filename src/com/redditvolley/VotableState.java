package com.redditvolley;
import org.json.*;

public class VotableState implements Votable
{

    private final int ups;
	private final int downs;
    private final Boolean isLiked;
	
	public VotableState(JSONObject obj) {
		this.ups = obj.optInt(Const.UPS);
		this.downs = obj.optInt(Const.DOWNS);
		Object tmpLikes = obj.opt(Const.LIKES);
		this.isLiked = tmpLikes == null ? null : obj.optBoolean(Const.LIKES);
	}
	
	public int getUps()
	{
		return ups;
	}

	public int getDowns()
	{
		return downs;
	}

	public Boolean getIsLiked()
	{
		return isLiked;
	}

}
