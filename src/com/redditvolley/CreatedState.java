package com.redditvolley;
import org.json.*;

public class CreatedState implements Created
{
    private final long created;
	private final long createdUtc;
	
	public CreatedState(JSONObject obj) {
		this.created = obj.optLong(Const.CREATED);
		this.createdUtc = obj.optLong(Const.CREATEDUTC);
	}

	public long getCreated()
	{
		return this.created;
	}

	public long getCreatedUtc()
	{
		return this.createdUtc;
	}
	
}
