package com.redditvolley;
import org.json.*;

public class Message
{
	final String	author;	
	final String	body;	//the message itself
	final String	bodyHtml;	//the message itself with html formatting
	final String	context;	//does not seem to return null but an empty string instead.
	final Integer	firstMessage;	//either null or the first message's ID represented as base 10 (wtf)
	final boolean	likes;	//how the logged-in user has voted on the message - True = upvoted, False = downvoted, null = no vote
	final String	linkTitle;	//if the message is actually a comment, contains the title of the thread it was posted in
	final String	name;	//ex: "t4_8xwlg"
	final boolean	isNew;	//unread? not sure
	final String	parentId;	//null if no parent is attached
	final String	replies;	//Again, an empty string if there are no replies.
	final String	subject;	//subject of message
	final String	subreddit;	//null if not a comment.
	final boolean	wasComment;
	
	public Message(JSONObject obj) {
		author = obj.optString(Const.AUTHOR);
		body = obj.optString(Const.BODY);
		bodyHtml = obj.optString(Const.BODYHTML);
		context = obj.optString(Const.CONTEXT);
		Object tmpFirstMsg = obj.opt(Const.FIRSTMESSAGE);
		firstMessage = tmpFirstMsg == null ? null : obj.optInt(Const.FIRSTMESSAGE);
		Object tmpLikes = obj.opt(Const.LIKES);
		likes = tmpLikes == null ? null : obj.optBoolean(Const.LIKES);
		linkTitle = obj.optString(Const.LINKTITLE);
		name = obj.optString(Const.NAME);
		isNew = obj.optBoolean(Const.ISNEW);
		parentId = obj.optString(Const.PARENTID);
		replies = obj.optString(Const.REPLIES);
		subject = obj.optString(Const.SUBJECT);
		subreddit = obj.optString(Const.SUBREDDIT);
		wasComment = obj.optBoolean(Const.WASCOMMENT);
	}
}
