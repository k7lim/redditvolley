package com.redditvolley;
import org.json.*;
import java.util.*;

public class More
{
   final JSONArray children;
   
   public More(JSONObject obj){
	   children = obj.optJSONArray(Const.CHILDREN);
   }
}
