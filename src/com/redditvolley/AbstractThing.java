package com.redditvolley;
import org.json.*;

public abstract class AbstractThing
{
   protected final String id;
   protected final String name;
   protected final String kind;
   
   // TODO design object for data
   
   AbstractThing(JSONObject obj) {
	   this.id = obj.optString(Const.ID);
	   this.name = obj.optString(Const.NAME);
	   this.kind = obj.optString(Const.TYPE);
   }
}
