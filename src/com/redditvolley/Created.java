package com.redditvolley;

public interface Created
{
  public long getCreated();
  public long getCreatedUtc();
}
