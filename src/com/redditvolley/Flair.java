package com.redditvolley;
import org.json.*;

public class Flair
{
   private final String cssClass;
   private final String text;
   
   public Flair(JSONObject obj, String prefix) {
	   this.cssClass = obj.optString(prefix + Const.FLAIRCSSCLASS);
	   this.text = obj.optString(prefix + Const.FLAIRTEXT);
   }
   
   public Flair(JSONObject obj) {
	   this(obj, "author");
   }

   public String getText()
   {
	   return text;
   }

   public String getCssClass()
   {
	   return cssClass;
   }
}
