package com.redditvolley;
import java.util.*;

public class Listing <T extends AbstractThing>
{
  
  final String before;
  final String after;
  final String modhash;
  private final List<T> data;
  
  Listing(String before, String after, String modhash, List<T> data) {
	  this.before = before;
	  this.after = after;
	  this.modhash = modhash;
	  this.data = data;
  }
  
  List<T> getData() {
	  return new ArrayList<T> (data);
  }
}
