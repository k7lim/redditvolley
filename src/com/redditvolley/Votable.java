package com.redditvolley;

public interface Votable {
  public int getUps();
  public int getDowns();
  public Boolean getIsLiked();
}
