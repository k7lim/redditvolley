package com.redditvolley;
import org.json.*;

public class Comment extends AbstractThing implements Votable, Created
{
   private final VotableState voteState;
   private final CreatedState createState;
   
	private final String	approvedBy; //	who approved this comment. null if nobody or you are not a mod
	private final String	author; //	the account name of the poster
	private final Flair	    authorFlair; //	the CSS class of the author's flair. subreddit specific
	// private final String	author_flair_text;	//the text of the author's flair. subreddit specific
	private final String	bannedBy;	//who removed this comment. null if nobody or you are not a mod
	private final String	body;	//the raw text. this is the unformatted text which includes the raw markup characters such as ** for bold. <, >, and & are escaped.
	private final String	bodyHtml;	//the formatted HTML text as displayed on reddit. For example, text that is emphasised by * will now have <em> tags wrapping it. Additionally, bullets and numbered lists will now be in html list format. NOTE: The html string will be escaped. You must unescape to get the raw html.
	private final Long	edited;	// false if not edited, edit date in UTC epoch-seconds otherwise
	private final int	gilded;	//the number of times this comment received reddit gold
	private final Boolean	likes;	//how the logged-in user has voted on the comment - True = upvoted, False = downvoted, null = no vote
	private final String	linkId;	//ID of the link this comment is in
	private final String	linkTitle;	//only present at /r/subreddit/comments/.json, contains what it says
	private final Integer	numReports;	//how many times this comment has been reported, null if not a mod
	private final String	parentId;	//ID of the thing this comment is a reply to, either the link or a comment in it
	private final boolean	scoreHidden;	//Whether the comment's score is currently hidden.
	private final String	subreddit;	//subreddit of thing excluding the /r/ prefix. "pics"
	private final String	subredditId;	//the id of the subreddit which is the thing is located in
	private final String	distinguished;	// to allow determining whether they have been distinguished by moderators/admins. null = not distinguished. moderator = the green [M]. admin = the red [A]. special = various other special distinguishes http://bit.ly/ZYI47B
   
   public Comment(JSONObject commentObj) {
	   super(commentObj);
	   voteState = new VotableState(commentObj);
	   createState = new CreatedState(commentObj);
       approvedBy = commentObj.optString(Const.APPROVEDBY);
	   author = commentObj.optString(Const.AUTHOR);
	   authorFlair = new Flair(commentObj); 
	   bannedBy = commentObj.optString(Const.BANNEDBY);
	   body = commentObj.optString(Const.BODY);
	   bodyHtml = commentObj.optString(Const.BODYHTML);
	   edited = commentObj.optLong(Const.EDITED);
	   gilded = commentObj.optInt(Const.GILDED);
	   Object likesTmp = commentObj.opt(Const.LIKES);
	   likes = likesTmp == null ? null: commentObj.optBoolean(Const.LIKES, true);
	   linkId = commentObj.optString(Const.LINKID);
	   linkTitle = commentObj.optString(Const.LINKTITLE);
	   Object numReportsTmp = commentObj.opt(Const.NUMREPORTS);
	   numReports = numReportsTmp == null ? null : commentObj.optInt(Const.NUMREPORTS);
	   parentId = commentObj.optString(Const.PARENTID);
	   scoreHidden = commentObj.optBoolean(Const.SCOREHIDDEN);
	   subreddit = commentObj.optString(Const.SUBREDDIT);
	   subredditId = commentObj.optString(Const.SUBREDDITID);
	   distinguished = commentObj.optString(Const.DISTINGUISHED);
    }

	public int getUps()
	{
       return voteState.getUps();
	}

	public int getDowns()
	{
		return voteState.getDowns();
	}

	public Boolean getIsLiked()
	{
		return voteState.getIsLiked();
	}

	public long getCreated()
	{
		return createState.getCreated();
	}

	public long getCreatedUtc()
	{
		return createState.getCreatedUtc();
	}


}
